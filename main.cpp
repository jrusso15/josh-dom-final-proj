/*
Domonique Carbajal and Josh Russo
Final Project
due 5/5/17
600.120
dcarbaj1@jhu.edu
jrusso15@jhu.edu
*/

#include "Game.h"
#include "Chess.h"

int main() {
    ChessGame chess;
    chess.setupBoard();
    chess.run();
}
