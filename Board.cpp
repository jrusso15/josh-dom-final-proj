/*
Domonique Carbajal and Josh Russo
Final Project
due 5/5/17
600.120
dcarbaj1@jhu.edu
jrusso15@jhu.edu
*/

#include <assert.h>
#include <cctype>
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>

#include "Game.h"
#include "Prompts.h"
#include "Chess.h" //added by Dom
using std::endl;
using std::cout;
using std::ofstream;
using std::ifstream;

///////////////
// Board //
///////////////

Board::~Board() {
    // Delete all pointer-based resources
    for (unsigned int i=0; i < m_width * m_height; i++)
        delete m_pieces[i];
    for (size_t i=0; i < m_registeredFactories.size(); i++)
        delete m_registeredFactories[i];
}

// Get the Piece at a specific Position, or nullptr if there is no
// Piece there or if out of bounds.
Piece* Board::getPiece(Position position)const {
    if (validPosition(position))
        return m_pieces[index(position)];
    else {
        if(getPrint()) {
            Prompts::outOfBounds();
        }
        return nullptr;
    }
}


// Create a piece on the board using the factory.
// Returns true if the piece was successfully placed on the board
bool Board::initPiece(int id, Player owner, Position position) {
    Piece* piece = newPiece(id, owner);
    if (!piece) return false;

    // Fail if the position is out of bounds
    if (!validPosition(position)) {
        Prompts::outOfBounds();
        return false;
    }
    // Fail if the position is occupied
    if (getPiece(position)) {
        Prompts::blocked();
        return false;
    }
    m_pieces[index(position)] = piece;
    return true;
}

void Board::printBoard() {
// print a current board
    int index = 0;
    int num=8;
    cout<< num<< "| ";
    for (std::vector<Piece*>::iterator it = m_pieces.begin(); it != m_pieces.end(); it++) {
        //char team;

        if (index % 8 == 0 && index != 0) {
            std::cout << std::endl;
            cout<< num<< "| ";
        }
        if (*it != nullptr) {
            std::cout << (*it)->owner() << (*it)->id() << " ";
        } else {
            std::cout << "-- ";
        }
        index++;
        num =8-(index/8);
    }
    std::cout << std::endl;
    cout<<"   -----------------------"<<endl;
    cout<<"   A  B  C  D  E  F  G  H"<<endl;

}

//moved here to include ChessGame declaration
//added by Dom 4/24
void Board::run() {
    std::string start, end,filename,loadfile;
    bool toggle = false;
    char choice = '0';
    while (choice != '1' && choice != '2') {
        Prompts::menu();
        std::cin >> choice;
    }
    if (choice == '2') {
        Prompts::loadGame();
        clearBoard();
        std::cin >> loadfile;
        if(!load(loadfile)) {
            exit(EXIT_FAILURE);
        }
    }
    while(1) {
        if (toggle) {
            printBoard();
        }
        //checked takes in index of piece so
        //can check if any position is 1 move from being captured
        //need index of king to do reg check
	
        setKI(kingInd());
	int kingIndex = getKI();
        if(kingIndex<0) {
            std::cout<<"checked() says no king on board"<<std::endl;
            break;
        }
//   cout<<"king is in position: "<<kingIndex<<endl;
        //added to tell you before turn
//      cout<<"the index putting in check is: "<<checked(kingIndex)<<endl;
        if(checked(kingIndex)) {
            Player chkr;
            if(playerTurn()) {
                chkr = WHITE;
            } else {
                chkr=BLACK;
            }
            Prompts::check(chkr);
            if(checkmate(kingIndex,checked(kingIndex))) {
                Prompts::checkMate(chkr);
                Prompts::gameOver();
                break;
            }
        }
        // checkmate(kingIndex,checked(kingIndex));
        if (m_turn % 2)  {
            Prompts::playerPrompt(WHITE, m_turn);
        } else {
            Prompts::playerPrompt(BLACK, m_turn);
        }
        std::cin >> start;
        std::transform(start.begin(), start.end(), start.begin(), ::tolower);
        if (start == "q") {
            Prompts::gameOver();

            break;
        } else if (start == "save") {
            Prompts::saveGame();
            std::cin>>filename;
            save(filename);
            break;
        } else if (start == "board") {
            if (toggle) {
                toggle = false;
                continue;
            } else {
                toggle = true;
                continue;
            }
        } else if (start == "forfeit") {
            if (playerTurn() == WHITE) {
                Prompts::win(BLACK, m_turn);
            } else {
                Prompts::win(WHITE, m_turn);
            }
            Prompts::gameOver();
            break;
        }
        std::cin >> end;
        std::transform(end.begin(), end.end(), end.begin(), ::tolower);

        if (!ChessGame::checkInput(start, end)) {
            Prompts::parseError();
            continue;
        } else {
            Position startpos = ChessGame::parseInput(start);
            Position endpos = ChessGame::parseInput(end);
            if (!validPosition(startpos) || !validPosition(endpos)) {
                Prompts::outOfBounds();
                continue;
            }

            setPrint(true);
            if(this->makeMove(startpos, endpos) > 0) {
                m_turn++;
                std::cout << "yay!" << std::endl;
            } else {

                continue;
            }
        }
    }
}

int Board::makeMove(Position start, Position end) {
  int kingIndex = getKI();
    int startindex = index(start);
    int endindex = index(end);

    if (m_pieces[startindex] == nullptr) {
        if(getPrint()) {
            Prompts::noPiece();
        }
        return -1;
    }
        if (playerTurn() != m_pieces[startindex]->owner()) {
	  if(getPrint()) {
            Prompts::noPiece();
        }
            return -1;
	    
        }
     else {
        int key = m_pieces[startindex]->validMove(start, end, *this);
        if (key > 0) {
            Piece* battle = getPiece(end);
            Player endOwn = WHITE;
            int endType = 0;
            if (battle !=nullptr) {
                endOwn = battle->owner();
                endType = battle->id();
            }
            //if it is a nullptr in endpos
            if(!battle) {
                m_pieces[endindex] = m_pieces[startindex];
                m_pieces[startindex] = nullptr;
                kingIndex= kingInd();
                if(checked(kingIndex)) {
                    m_pieces[startindex]= m_pieces[endindex];
                    m_pieces[endindex] = nullptr;
                    if(getPrint()) {
                        Prompts::mustHandleCheck();
                    }
                    return -1;
                }
            } else {
                if (battle->owner() == m_pieces[startindex]->owner()) {
                    if(getPrint()) {
                        Prompts::blocked();
                    }
                    return -1;
                } else {
                    delete m_pieces[endindex];
                    m_pieces[endindex] = m_pieces[startindex];
                    m_pieces[startindex] = nullptr;
                    kingIndex = kingInd();
                    if(checked(kingIndex)) {
                        m_pieces[startindex]= m_pieces[endindex];
                        delete m_pieces[endindex];
                        initPiece(endType,endOwn,end);
                    } else {
                        if(getPrint()) {
                            Prompts::capture(playerTurn());
                        }
                    }
                }
            }
            if (getPiece(end)->id() == 0) {
                getPiece(end)->becomeQueen(end, *this);
            }
        } else if (key == MOVE_ERROR_BLOCKED) {
            if(getPrint()) {
                Prompts::blocked();
            }
            return -1;
        } else if (key == MOVE_ERROR_ILLEGAL) {
            if(getPrint()) {
                Prompts::illegalMove();
            }
            return -1;
        }
    }
    return SUCCESS;
}



// Add a factory to the Board to enable producing
// a certain type of piece
bool Board::addFactory(AbstractPieceFactory* pGen) {
    // Temporary piece to get the ID
    Piece* p = pGen->newPiece(WHITE);
    int id = p->id();
    delete p;

    PieceGenMap::iterator it = m_registeredFactories.find(id);
    if (it == m_registeredFactories.end()) { // not found
        m_registeredFactories[id] = pGen;
        return true;
    } else {
        std::cout << "Id " << id << " already has a generator\n";
        return false;
    }
}

// Search the factories to find a factory that can translate `id' to
// a Piece, and use it to create the Piece. Returns nullptr if not found.
Piece* Board::newPiece(int id, Player owner) {
    PieceGenMap::iterator it = m_registeredFactories.find(id);
    if (it == m_registeredFactories.end()) { // not found
        std::cout << "Id " << id << " has no generator\n";
        return nullptr;
    } else {
        return it->second->newPiece(owner);
    }
}
///////////////////////////////////////
int Board::simpleBound(int x, int y) {
    if(x < 0|| x>7||y < 0|| y >7) {
        return 0;
    }
    else {
        return 1;
    }
}

int Board::checkChecked(int x, int y, int type) {
    Position pos = Position(x,y);
    Player play = playerTurn();
    if(simpleBound(x,y)) {
        int nindex = index(pos);
        if(m_pieces[nindex] != nullptr) {
            if(m_pieces[nindex]->owner() != play && m_pieces[nindex]->id()==type) {
                return nindex;
            } else {
                return 0;
            }
        }
        else {
            return 0;
        }
    }
    else {
        return 0;
    }

}

int Board::checkBish(Position kpos, int mvx, int mvy) {
    int x= kpos.x+mvx;
    int y = kpos.y+mvy;
    while(simpleBound(x,y)) {
        kpos.x = x;
        kpos.y = y;
        if(checkChecked(x,y, 3)) {
            //	  cout<<"Error 1"<<endl;
            return checkChecked(x,y, 3) ;
        }
        if(checkChecked(x,y, 4)) {
            //  cout<<"error 2"<<endl;
            return checkChecked(x,y, 4);
        }
        if(m_pieces[index(kpos)] != nullptr) {
            // cout<<"Okay 3"<<endl;
            return 0;
        }
        x+=mvx;
        y+=mvy;
    }
    return 0;
}

int Board::checkRook(Position kpos, int mvx, int mvy) {
    int x= kpos.x+mvx;
    int y = kpos.y+mvy;
    while(simpleBound(x,y)) {
        kpos.x = x;
        kpos.y = y;
        if(checkChecked(x,y, 1)) {
            //	  cout<<"Error 1"<<endl;
            return checkChecked(x,y, 1);
        }
        if(checkChecked(x,y, 4)) {
            //	  cout<<"error 2"<<endl;
            return checkChecked(x,y, 4);
        }
        if(m_pieces[index(kpos)] != nullptr) {
            //	   cout<<"Okay 3"<<endl;
            return 0;
        }
        x+=mvx;
        y+=mvy;
    }
    return 0;
}

int Board::checker(int check1, int check2, int check3, int check4) {
    if(check1||check2||check3||check4) {
        if(check1) {
            return check1;
        }
        else if(check2) {
            return check2;
        }
        else if(check3) {
            return check3;
        }
        else if(check4) {
            return check4;
        }
        return 1;
    }
    else {
        return 0;
    }
}


//will return True(1) if player is checked
int Board::checked(int index) {
    Player play = playerTurn();
    //need to know which player is playing
    //need to search for where their king is/the index

    Position kingpos = Position(index%8,index/8);
    int kingx= index%8;
    int kingy= index/8;

    //check if bound 2 right and 1 down exists
    //if so checks if it is THE OTHER PLAYER KNIGHT(2)
    int check1 = checkChecked(kingx+2,kingy +1,2);
    int check2 = checkChecked(kingx +2,kingy -1,2);
    int check3= checkChecked(kingx -2,kingy -1,2);
    int check4=checkChecked(kingx -2,kingy +1,2) ;

    int finalcheck =  checker(check1,check2, check3,check4);
    if( finalcheck) {
        return  finalcheck;
    }

    check1 = checkChecked(kingx +1,kingy +2,2);
    check2 = checkChecked(kingx +1,kingy -2,2);
    check3= checkChecked(kingx -1,kingy -2,2);
    check4=checkChecked(kingx -1,kingy +2,2);
    finalcheck =  checker(check1, check2, check3, check4);
    if( finalcheck) {
        return  finalcheck;
    }

    //check if the pawn is diag to you(can only move forward so depends on player)
    check1=checkChecked(kingx+1,kingy+1,0);
    check2=checkChecked(kingx-1,kingy+1,0);
    check3=checkChecked(kingx-1,kingy-1,0);
    check4=checkChecked(kingx+1,kingy-1,0);

    if (play) {

        if(check1||check2) {
            if(check1) {
                return check1;
            }
            else if(check2) {
                return check2;
            }
            return 1;
        }
    }
    else {
        if(check3||check4) {
            if(check3) {
                return check3;
            }
            else if(check4) {
                return check4;
            }
            return 1;
        }
    }
    //check if the bishop or queen are diagonal to you
    //in any diagonal (CAN stop checking if you hit anything else)

//kings position and what x,y are going to be modified by (+-1)

    check1 = checkBish(kingpos,1,1);
    check2 = checkBish(kingpos,1,-1);
    check3= checkBish(kingpos,-1,1);
    check4= checkBish(kingpos,-1,-1);

    finalcheck =  checker(check1, check2, check3, check4);
    if( finalcheck) {
        return  finalcheck;
    }

    //check 0r rook or queen are horizontal or vertical
//to the king, stop in 1d if find anything in the way stops
    check1 =checkRook(kingpos,0,1);
    check2=checkRook(kingpos,0,-1);
    check3=checkRook(kingpos,1,0);
    check4=checkRook(kingpos,-1,0);

    finalcheck =  checker(check1, check2, check3, check4);
    if( finalcheck) {
        return  finalcheck;
    }

    return 0;
}

int Board::save(string filename) {

    std::map<int, char> alphaCode = {{0,'a'},{1,'b'},{2,'c'},{3,'d'},{4,'e'},{5,'f'},{6,'g'},{7,'h'}};

    ofstream svfile;
    svfile.open(filename);
    if (!svfile.is_open()) {
        cout<<"Error: Unable to Open File"<<endl;
        return 1;
    }
    svfile<<"chess"<<endl;
    svfile<< m_turn <<endl;
    int index=0;
    for (std::vector<Piece*>::iterator it = m_pieces.begin(); it != m_pieces.end(); it++) {
        if (*it != nullptr) {
            svfile<< (*it)->owner()<<" "<< alphaCode[index%8] <<(8-(index/8)) <<" "<<(*it)->id() <<endl;

        }
        index++;
    }
    svfile.close();
    return 0;
}


int Board::load(string filename) {
    string word,newturn,input;
    int turn, player,type;
    Player temp;
    //open file
    ifstream opfile(filename);
    //check file valid
    if(!opfile.is_open()) {
        //prompts thing
        Prompts::loadFailure();
        return 0;
    }
    //check chess as first line
    opfile>>word;
    if(word != "chess") {
        Prompts::loadFailure();
        return 0;
    }
    //set 2nd line count equal to m_turn
    opfile>>newturn;
    for(unsigned int i=0; i<newturn.length(); i++) {
        if (!isdigit(newturn.at(i))) {
            Prompts::loadFailure();
            return 0;
        }
    }
    turn = stoi(newturn,nullptr);
    m_turn = turn;

    //for each preceeding line
    //1st = player, 2nd = parseInput to get pos 3rd=type
    //use piece init to put onto board(player pos type)
    while(opfile>>player>>input>>type) {
        if(player) {
            temp = BLACK;
        }
        else {
            temp = WHITE;
        }
        Position pcpos= ChessGame::parseInput(input);
        initPiece(type,temp,pcpos);
    }

    return 1;
    //If ANY fail then return 0(false)
}

void Board::clearBoard() {
    for (std::vector<Piece*>::iterator it = m_pieces.begin(); it != m_pieces.end(); it++) {
        if (*it != nullptr) {
            delete *it;
            *it = nullptr;
        }
    }
}

int Board::kingInd() {
    int posit = 0;
    int kingIndex = -1;
    for (std::vector<Piece*>::iterator it = m_pieces.begin(); it != m_pieces.end(); it++) {
        if(*it != nullptr) {
            if(((*it)->owner() == (int)playerTurn()) && ((*it)->id() == 5)) {
                kingIndex= posit;
                break;
            }
        }
        posit++;
    }

    return kingIndex;
}

int Board::kingInd2() {
    int posit = 0;
    int kingIndex = -1;
    for (std::vector<Piece*>::iterator it = m_pieces.begin(); it != m_pieces.end(); it++) {
        if(*it != nullptr) {
            if(((*it)->owner() != (int)playerTurn()) && ((*it)->id() == 5)) {
                kingIndex= posit;
                break;
            }
        }
        posit++;
    }

    return kingIndex;
}
vector<int> Board::checkpath(int kindex, const int cindex) {
    //check type, find path, check each point in path
    vector<int> path;
    int ctype = m_pieces[cindex]->id();
    int kx= kindex%8;
    int ky= abs(7-(kindex/8));
    int cx= cindex%8;
    int cy= abs(7-cindex/8);
    int move = ky-cy;
    int move2 = kx-cx;
    int temp= cindex;
    /////Pawn and Knight/////
    if (ctype == 2|| ctype == 0) {
        path.push_back(cindex);
    }
/////////Rook & Queen//////
    //if rook moves horiz or vertical
    if (ctype == 1|| ctype==4) {
        if(move && !move2) {
            if(move<0) {
                temp= cindex;
                for(int i=0; i<abs(move); i++) {
                    path.push_back(temp);
                    temp-=8;
                }
            }
            else if(move>0) {
                temp= cindex;
                for(int i=0; i<abs(move); i++) {
                    path.push_back(temp);
                    temp+=8;
                }
            }
        }
        else if(move2 && !move) {

            temp= cindex;
            if(move2 >0) {
                for(int i=0; i<abs(move2); i++) {
                    path.push_back(temp);
                    temp+=1;
                }
            }
            else if(move2<0) {
                temp= cindex;
                if(move2 <0) {
                    for(int i=0; i<abs(move2); i++) {
                        path.push_back(temp);
                        temp-=1;
                    }
                }
            }
        }
    }
    ///////Bishop&queen////////
    if (ctype == 3|| ctype==4) {
        //moves in the positive y direction (+9/+7)
        if(move>0) {
            if(move2<0) {
                int temp= cindex;
                for(int i=0; i<abs(move); i++) {
                    path.push_back(temp);
                    temp+=7;
                }
            }
            else if(move2>0) {
                temp= cindex;
                for(int i=0; i<abs(move); i++) {
                    path.push_back(temp);
                    temp+=9;
                }
            }
        }

        else if(move<0) {
            temp= cindex;
            if(move2 >0) {
                for(int i=0; i<abs(move2); i++) {
                    path.push_back(temp);
                    temp-=7;
                }
            }
            else if(move2<0) {
                temp= cindex;
                for(int i=0; i<abs(move2); i++) {
                    path.push_back(temp);
                    temp-=9;
                }
            }
        }
    }
    /*
    if(!path.empty()){
    for (vector<int>::const_iterator j = path.begin();
        j!=path.end(); ++j) {
                  cout<<*j<<" ";
    }
    }
    cout<<endl;
    */
    return path;

}
//returns false if there is not a checkmate
int Board::checkmate(int kindex, const int cindex) {
  kindex = kingInd2();
  //Position kingpos = Position(kindex%8,kindex/8);
    int kingx= kindex%8;
    int kingy= kindex/8;
    // setPrint(false);
    //see if king can avoid check by moving 1 space
    for(int i=-1; i<2; i++) {
        for(int j=-1; j<2; j++) {
            Position end = Position(kingx+j,kingy+i);
	    cout<<(kindex/8)<<endl;
            if(makeMove(kindex, end) > 0) {
	     
	      return 0;
            }
        }
    }
    //method that returns vector of all the indices of the position
    //in the path putting in check
    vector<int> path = checkpath(kindex,cindex);
    //for each possible position see if you can move any piece into there
 setPrint(false);
    if(!path.empty()) {
        for (vector<int>::const_iterator j = path.begin();
                j!=path.end(); ++j) {
            int indx=0;
            for (std::vector<Piece*>::iterator it = m_pieces.begin(); it != m_pieces.end(); it++) {
                if(*it != nullptr) {
                    if((*it)->owner() ==playerTurn() && (*it)->id()!= 5) {
                        Position end2 = Position((*j)%8,(*j)/8);
                        if(makeMove(indx, end2) > 0) {
                            setPrint(true);
                            return 0;
                        }
                    }
                }
                indx++;
            }
        }
    }
    setPrint(true);
    return 1;
}

