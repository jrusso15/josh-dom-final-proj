CXX = g++
CXXFLAGS = -Wall -Wextra -pedantic -std=c++11 -g

chess: main.o Chess.o Board.o
	$(CXX) $(CXXFLAGS) main.o Chess.o Board.o -o chess

unittest: unittest.o Chess.o Board.o
	$(CXX) $(CXXFLAGS) unittest.o Chess.o Board.o -o unittest

Board.o: Game.h
Chess.o: Game.h Chess.h Prompts.h
main.o: Game.h Chess.h Prompts.h
unittest.o: Game.h Chess.h Prompts.h

clean:
	rm *.o chess
	rm unittest

