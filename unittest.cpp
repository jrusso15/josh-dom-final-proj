/*
Domonique Carbajal and Josh Russo
Final Project
due 5/5/17
600.120
dcarbaj1@jhu.edu
jrusso15@jhu.edu
*/

#include "Game.h"
#include "Chess.h"
#include <iostream>

bool checkPawnValid(ChessGame chess) {
    chess.load(std::string("testpawn.txt"));
    if (chess.getPiece({2,4})->validMove({2,4}, {2,3}, chess) != SUCCESS) {
        return false;
    }
    if (chess.getPiece({2,4})->validMove({2,4}, {1,4}, chess) == SUCCESS) {
        return false;
    }
    if (chess.getPiece({2,4})->validMove({2,4}, {2,5}, chess) == SUCCESS) {
        return false;
    }
    if (chess.getPiece({2,4})->validMove({2,4}, {1,3}, chess) == SUCCESS) {
        return false;
    }
    chess.clearBoard();
    return true;
}

bool checkDiagValid(ChessGame chess) {
    chess.load(std::string("testdiag.txt"));
    if (chess.getPiece({4,5})->validMove({4,5}, {2,3}, chess) != SUCCESS) {
        return false;
    }
    if (chess.getPiece({4,5})->validMove({4,5}, {6,2}, chess) == SUCCESS) {
        return false;
    }
    if (chess.getPiece({4,5})->validMove({4,5}, {5,2}, chess) == SUCCESS) {
        return false;
    }
    chess.clearBoard();
    return true;
}

bool checkLineValid(ChessGame chess) {
    chess.load(std::string("testline.txt"));
    if (chess.getPiece({3,4})->validMove({3,4}, {1,4}, chess) != SUCCESS) {
        return false;
    }
    if (chess.getPiece({3,4})->validMove({3,4}, {4,3}, chess) == SUCCESS) {
        return false;
    }
    if (chess.getPiece({3,7})->validMove({3,7}, {3,5}, chess) != SUCCESS) {
        return false;
    }
    if (chess.getPiece({3,4})->validMove({3,4}, {3,6}, chess) != SUCCESS) {
        return false;
    }
    chess.clearBoard();
    return true;
}

bool checkKnightValid(ChessGame chess) {
    chess.load(std::string("fresh.txt"));
    if (chess.getPiece({1,7})->validMove({1,7}, {2,5}, chess) != SUCCESS) {
        return false;
    }
    if (chess.getPiece({1,7})->validMove({1,7}, {2,6}, chess) == SUCCESS) {
        return false;
    }
    if (chess.getPiece({1,7})->validMove({1,7}, {0,5}, chess) != SUCCESS) {
        return false;
    }
    chess.clearBoard();
    return true;
}

bool checkCheckInput(ChessGame chess) {
    if (chess.checkInput(std::string("a7"), std::string("a3")) != true) {
        return false;
    }
    if (chess.checkInput(std::string("hi"), std::string("yolo")) == true) {
        return false;
    }
    if (chess.checkInput(std::string("b8"), std::string("c4")) != true) {
        return false;
    }
    if (chess.checkInput(std::string("59823"), std::string("9319")) == true) {
        return false;
    }
    return true;
}

int main() {
    ChessGame chess;
    if (checkCheckInput(chess)) {
        std::cout << "check input test passed" << std::endl;
    } else {
        std::cout << "check input test failed" << std::endl;
    }
    if (checkPawnValid(chess)) {
        std::cout << "pawnvalid test passed" << std::endl;
    } else {
        std::cout << "pawnvalid test failed" << std::endl;
    }
    if (checkDiagValid(chess)) {
        std::cout << "diagonal valid test passed" << std::endl;
    } else {
        std::cout << "diagonal valid test failed" << std::endl;
    }
    if (checkLineValid(chess)) {
        std::cout << "line valid test passed" << std::endl;
    } else {
        std::cout << "line valid test failed" << std::endl;
    }
    if (checkKnightValid(chess)) {
        std::cout << "knight valid test passed" << std::endl;
    } else {
        std::cout << "knight valid test failed" << std::endl;
    }
    return 0;
}
