/*
Domonique Carbajal and Josh Russo
Final Project
due 5/5/17
600.120
dcarbaj1@jhu.edu
jrusso15@jhu.edu
*/

#undef FOR_RELEASE

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"

// Make a move on the board. Return an int, with < 0 being failure
int ChessGame::makeMove(Position start, Position end) {
    // Possibly implement chess-specific move logic here
    //
    // We call Board's makeMove to handle any general move logic
    // Feel free to use this or change it as you see fit
    int retCode = Board::makeMove(start, end);
    return retCode;
}

// Setup the chess board with its initial pieces
void ChessGame::setupBoard() {
    std::vector<int> pieces {
        ROOK_ENUM, KNIGHT_ENUM, BISHOP_ENUM, QUEEN_ENUM,
        KING_ENUM, BISHOP_ENUM, KNIGHT_ENUM, ROOK_ENUM
    };
    for (size_t i = 0; i < pieces.size(); ++i) {
        initPiece(PAWN_ENUM, BLACK, Position(i, 1));
        initPiece(pieces[i], BLACK, Position(i, 0));
        initPiece(pieces[i], WHITE, Position(i, 7));
        initPiece(PAWN_ENUM, WHITE, Position(i, 6));
    }
}

//method to change a pawn into a queen when it hits the opposite end of the board
bool ChessPiece::becomeQueen(Position end, Board& board) const {
    if (this->owner() == BLACK && end.y == 7) {
        delete this;
        return board.initPiece(QUEEN_ENUM, BLACK, end);
    } else if (this->owner() == WHITE && end.y == 0) {
        delete this;
        return board.initPiece(QUEEN_ENUM, WHITE, end);
    }
    return false;
}

//checks to see if input is valid
bool ChessGame::checkInput(std::string start, std::string end) {
    if (start.at(0) < 'a' || start.at(0) > 'h') {
        return false;
    }
    if (end.at(0) < 'a' || end.at(0) > 'h') {
        return false;
    }
    if (start.at(1) < '1' || start.at(1) > '8') {
        return false;
    }
    if (end.at(1) < '1' || end.at(1) > '8') {
        return false;
    }
    return true;
}

//checks end pos for a piece, if it's the owners or not
int ChessPiece::checkEnd(Position end, const Board& board) const {
    if (board.getPiece(end)) {
        if (board.getPiece(end)->owner() == this->owner()) {
            return MOVE_ERROR_BLOCKED;
        }
    }
    return SUCCESS;
}

int Pawn::validMove(Position start, Position end, const Board& board) const {
    int boundearly, boundlate; //need these var to distinguish up from down
    if (this->owner() == WHITE) {
        boundearly = -2;
        boundlate = -1;
    } else {
        boundearly = 2;
        boundlate = 1;
    }
    if (start.x == end.x + 1) {
        if (board.getPiece(end)) {
            if (board.getPiece(end)->owner() != this->owner()) {
                return SUCCESS;
            }
            return MOVE_ERROR_BLOCKED;
        }
    } else if (start.x == end.x - 1) {
        if (board.getPiece(end)) {
            if (board.getPiece(end)->owner() != this->owner()) {
                return SUCCESS;
            }
            return MOVE_ERROR_BLOCKED;
        }
    } else if (start.x == end.x) {
        if (board.turn() > 2) {
            if ((int (end.y - start.y)) != boundlate) {
                return MOVE_ERROR_ILLEGAL;
            }
        }
        if (board.turn() < 2) {
            if (!(((int) end.y - (int) start.y) != boundlate || (((int) end.y - (int) start.y) != boundearly))) {
                return MOVE_ERROR_ILLEGAL;
            }
        }
        if (board.getPiece(end)) {
            return MOVE_ERROR_ILLEGAL;
        }
        return SUCCESS;
    }
    return MOVE_ERROR_ILLEGAL;
}

//if move is diagonal (bishop or queen), check the move
int ChessPiece::checkDiag(Position start, Position end, const Board& board) const {
    int starty = (int) start.y;
    int startx = (int) start.x;
    int endy = (int) end.y;
    int endx = (int) end.x;
    unsigned int j;
    if (ChessPiece::checkEnd(end, board) == MOVE_ERROR_BLOCKED) {
        return MOVE_ERROR_BLOCKED;
    }
    if (std::abs(startx - endx) != std::abs(starty - endy)) {
        return MOVE_ERROR_ILLEGAL;
    }
    if (start.x < end.x) {
        if (start.y < end.y) {
            j = start.y + 1;
            for (unsigned int i = start.x + 1; i < end.x; i++) {
                if (board.getPiece({i , j})) {
                    return MOVE_ERROR_BLOCKED;
                }
                j++;
            }
        } else {
            j = start.y - 1;
            for (unsigned int i = start.x + 1; i < end.x; i++) {
                if (board.getPiece({i , j})) {
                    return MOVE_ERROR_BLOCKED;
                }
                j--;
            }
        }
    } else {
        if (start.y < end.y) {
            j = start.y + 1;
            for (unsigned int i = start.x - 1; i > end.x; i--) {
                if (board.getPiece({i , j})) {
                    return MOVE_ERROR_BLOCKED;
                }
                j++;
            }
        } else {
            j = start.y - 1;
            for (unsigned int i = start.x - 1; i > end.x; i--) {
                if (board.getPiece({i , j})) {
                    return MOVE_ERROR_BLOCKED;
                }
                j--;
            }
        }
    }
    return SUCCESS;
}

//for rook and queen
int ChessPiece::checkLine(Position start, Position end, const Board& board) const {
    if (ChessPiece::checkEnd(end, board) == MOVE_ERROR_BLOCKED) {
        return MOVE_ERROR_BLOCKED;
    }
    if (start.y != end.y && start.x != end.x) {
        return MOVE_ERROR_ILLEGAL;
    }
    if (start.y == end.y) {
        if (start.x < end.x) {
            for (unsigned int i = start.x + 1; i < end.x; i++) {
                if (board.getPiece({i, start.y})) {
                    return MOVE_ERROR_BLOCKED;
                }
            }
        } else if (start.x > end.x) {
            for (unsigned int i = start.x - 1; i > end.x; i--) {
                if (board.getPiece({i, start.y})) {
                    return MOVE_ERROR_BLOCKED;
                }
            }
        }
    } else if (start.x == end.x) {
        if (start.y < end.y) {
            for (unsigned int i = start.y + 1; i < end.y; i++) {
                if (board.getPiece({start.x, i})) {
                    return MOVE_ERROR_BLOCKED;
                }
            }
        } else if (start.y > end.y) {
            for (unsigned int i = start.y - 1; i > end.y; i--) {
                if (board.getPiece({start.x, i})) {
                    return MOVE_ERROR_BLOCKED;
                }
            }
        }
    }
    return SUCCESS;
}

int Rook::validMove(Position start, Position end, const Board& board) const {
    return ChessPiece::checkLine(start, end, board);
}

int Knight::validMove(Position start, Position end, const Board& board) const {
    int starty = (int) start.y;
    int startx = (int) start.x;
    int endy = (int) end.y;
    int endx = (int) end.x;
    if (ChessPiece::checkEnd(end, board) == MOVE_ERROR_BLOCKED) {
        return MOVE_ERROR_BLOCKED;
    }
    if (std::abs(starty - endy) == 2) {
        if (std::abs(startx - endx) == 1) {
            return SUCCESS;
        }
    } else if (std::abs(startx - endx) == 2) {
        if (std::abs(starty - endy) == 1) {
            return SUCCESS;
        }
    }
    return MOVE_ERROR_ILLEGAL;
}

int Bishop::validMove(Position start, Position end, const Board& board) const {
    return ChessPiece::checkDiag(start, end, board);
}

//combo of checking diagonal and lines
int Queen::validMove(Position start, Position end, const Board& board) const {
    if (ChessPiece::checkDiag(start, end, board) > 0 || ChessPiece::checkLine(start, end, board) > 0) {
        return SUCCESS;
    }
    return MOVE_ERROR_ILLEGAL;

}

//can only move one square in each direction
int King::validMove(Position start, Position end, const Board& board) const {
    if (ChessPiece::checkEnd(end, board) == MOVE_ERROR_BLOCKED) {
        return MOVE_ERROR_BLOCKED;
    }
    
    int starty = (int) start.y;
    int startx = (int) start.x;
    int endy = (int) end.y;
    int endx = (int) end.x;
    if (std::abs(starty - endy) == 1 || starty - endy == 0) {
        if (startx - endx == 0 || std::abs(startx - endx) == 1) {
            return SUCCESS;
        }
    }
    return MOVE_ERROR_ILLEGAL;
}

//changes string input into positions
Position ChessGame::parseInput(std::string location) {
    unsigned int x, y;
    x = (location.at(0) - 'a');
    y = 7 - (location.at(1) - '1');
    Position pos = Position(x , y);
    return pos;
}

